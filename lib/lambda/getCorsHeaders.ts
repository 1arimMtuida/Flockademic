// Use a polyfill for URL since Node has adopted it for Node 8 as well
// (and we're still at Node 6 at the time of writing).
// tslint:disable-next-line:no-reference
/// <reference path="whatwg-url.d.ts"/>
import { URL } from 'whatwg-url';

// The following is imported from @types/aws-lambda, so we don't need the actual dependency:
// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayEvent } from 'aws-lambda';

interface ResponseHeaders {
  [key: string]: string;
}

export function getCorsHeaders(requestHeaders: APIGatewayEvent['headers'] | null = {}): ResponseHeaders {
  if (requestHeaders === null) {
    requestHeaders = {};
  }
  const originHeaders = Object.keys(requestHeaders).filter((header) => header.toLowerCase() === 'origin');
  const responseHeaders: ResponseHeaders = {};
  if (originHeaders.length > 0) {
    const originValue = requestHeaders[originHeaders[0]];
    const origin = new URL(originValue);
    if (/flockademic\.com$/.test(origin.hostname)) {
      responseHeaders['Access-Control-Allow-Origin'] = originValue;
    }
  }

  return responseHeaders;
}
