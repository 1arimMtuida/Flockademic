import { Account } from '../Account';
import { Person } from '../Person';
import { Session } from '../Session';

export interface GetProfileResponse {
  profiles: Person[];
}

export interface GetAccountIdResponse {
  identifier: string;
}

export interface PostNewSessionResponse {
  session: Session;
  jwt: string;
  refreshToken: string;
}

export interface PostRefreshJwtRequest {
  refreshToken: string;
}
export interface PostRefreshJwtResponse {
  jwt: string;
}

export interface PostVerifyOrcidRequest {
  jwt: string;
  redirectUri: string;
}

export interface PostVerifyOrcidResponse {
  account: Account & { orcid: string; };
  jwt: string;
}
