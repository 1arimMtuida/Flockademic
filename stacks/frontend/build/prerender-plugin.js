const fs = require('fs');

const React = require('react');
const ReactDOM = require('react-dom');
const ReactDOMServer = require('react-dom/server');

class PrerenderPlugin{
  constructor(options){
    let defaultOptions = {
      // Note that the placeholder needs to have this exact form, *after* compilation
      // (in other words, take into account that it might have been minified)
      placeholder: 'Loading&hellip; (Requires Javascript)',
      // Note that the element will need to be output as UMD to enable this plugin to
      // require it. In other words, in your Webpack config, set
      // config.output.libraryTarget to `umd`.
      elementOutputFile: /app(.*).js/,
      htmlOutputFile: /index.html/,
      // This one will be used for rendering the fallback 404 page
      fallbackOutputFile: /fallback.html/,
    };

    this.options = Object.assign(defaultOptions, options);
  }

  apply(compiler){
    // TODO: Somehow get a hold of the compiled app before the HTMLWebpackPlugin's
    // `html-webpack-plugin-before-html-processing` phase, and insert it into
    // htmlPluginData.html there.
    // This will allow the compiled output to get minified as well.
    // For more info, see https://github.com/ampedandwired/html-webpack-plugin#events
    compiler.plugin("after-emit", compilation => {
      const appFilePath = getAssetPath(compilation.assets, this.options.elementOutputFile);
      // The `.default` is needed because it's an ES2015 module
      const App = require(appFilePath).default;

      const AppElement = React.createElement(App, { renderIndex: true });

      const htmlFilePath = getAssetPath(compilation.assets, this.options.htmlOutputFile);
      let content = fs.readFileSync(htmlFilePath, 'utf8');
      fs.writeFileSync(
        htmlFilePath,
        content.replace(this.options.placeholder, ReactDOMServer.renderToString(AppElement))
      );

      // Whereas the regular HTML file includes a fully rendered landing page,
      // for non-root pages we want a file that only includes the app shell:
      const FallbackAppElement = React.createElement(App, { renderIndex: false });

      const fallbackHtmlFilePath = getAssetPath(compilation.assets, this.options.fallbackOutputFile);
      let fallbackContent = fs.readFileSync(fallbackHtmlFilePath, 'utf8');
      fs.writeFileSync(
        fallbackHtmlFilePath,
        fallbackContent.replace(this.options.placeholder, ReactDOMServer.renderToString(FallbackAppElement))
      );
    });
  }
}

function getAssetPath(assets, assetName){
  const assetHash = Object.keys(assets).filter(asset => assetName.test(asset))[0];

  return assets[assetHash].existsAt;
}

module.exports = PrerenderPlugin;
