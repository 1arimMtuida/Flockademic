import './styles.scss';

import * as React from 'react';
import * as ReactGA from 'react-ga';
import { Redirect, RouteComponentProps } from 'react-router';

import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { getWork as getOrcidWork } from '../../services/orcid';
import { initialiseArticle } from '../../services/periodical';
import { Spinner } from '../spinner/component';

export interface ArticleInitialisationPageState {
  slug: string | undefined | null;
}
export interface ArticleInitialisationPageRouteParams {
  orcidWorkId?: string;
  periodicalSlug?: string;
}

export class ArticleInitialisationPage extends React.Component<
  RouteComponentProps<ArticleInitialisationPageRouteParams> | undefined,
  ArticleInitialisationPageState
> {
  public state: ArticleInitialisationPageState = {
    slug: undefined,
  };

  public async componentDidMount() {
    try {
      let initialArticle: undefined | Partial<ScholarlyArticle>;

      if (
        this.props.match.params.orcidWorkId
        && /^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X):\d+$/.test(this.props.match.params.orcidWorkId)
      ) {
        ReactGA.event({
          action: 'Initialise new article based on an ORCID work',
          category: 'Article Management',
        });

        const [ orcid, putCode ] = this.props.match.params.orcidWorkId.split(':');

        initialArticle = await getOrcidWork(orcid, putCode);
        // We don't really care about other locations ORCID knows about now that the article is on Flockademic,
        // so we can simply link this to the ORCID work.
        // If necessary, we can always look it up again through the ORCID API.
        initialArticle.sameAs = `https://orcid.org/${orcid}/work/${putCode}`;
      } else {
        ReactGA.event({
          action: 'Initialise new article',
          category: 'Article Management',
        });
      }
      const response = await initialiseArticle(this.props.match.params.periodicalSlug, initialArticle);
      this.setState({ slug: response.result.identifier });
    } catch (e) {
      this.setState({ slug: null });
    }
  }

  public render() {
    return (
      <div className="section">
        <section className="section__primary-content">
          <div className="article">
            {this.getView()}
          </div>
        </section>
      </div>
    );
  }

  private getView() {
    if (typeof this.state.slug === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.slug === null) {
      return (
        <div className="callout alert">
          There was a problem loading the submission page, please refresh to try again.
        </div>
      );
    }

    return <Redirect to={`/article/${this.state.slug}/manage`}/>;
  }
}
