import './styles.scss';

import * as React from 'react';
import { OutboundLink } from 'react-ga';
import { Link } from 'react-router-dom';

import { Periodical } from '../../../../../lib/interfaces/Periodical';
import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../../lib/interfaces/Session';
import { isPeriodicalOwner } from '../../../../../lib/utils/periodicals';
import { ArticleList } from '../articleList/component';
import { PageMetadata } from '../pageMetadata/component';

interface JournalOverviewProps {
  periodical: Partial<Periodical>;
  url: string;
  articles?: null | Array<Partial<ScholarlyArticle>>;
  session?: Session;
}

export class JournalOverview
  extends React.Component<
  JournalOverviewProps,
  {}
> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div itemScope={true} itemType="https://schema.org/Periodical">
        <section className="hero is-medium is-primary is-bold" style={this.getHeroStyles()}>
          <div className="hero-body">
            <div className="container">
              <PageMetadata
                url={this.props.url}
                title={this.props.periodical.name}
                description={this.props.periodical.description}
              />
              <h1 itemProp="name" className="title">{this.props.periodical.name}</h1>
              {this.renderHeadline()}
            </div>
          </div>
          <div className="hero-foot">
            <nav role="navigation" aria-label="Journal navigation" className="tabs is-boxed is-right is-medium">
              <div className="container">
                <ul>
                  {this.renderSettingsLink()}
                </ul>
              </div>
            </nav>
          </div>
        </section>
        {this.renderSuggestion()}
        <section className="section">
          <div className="container">
            <div className="columns">
              <div className="column is-half-widescreen">
                {this.renderDescription()}
              </div>
              <div className="column is-half-widescreen has-text-centered">
                {this.renderSubmissionButton()}
              </div>
            </div>
          </div>
        </section>
        {this.renderBoard()}
        {this.renderArticles()}
      </div>
    );
  }

  private getHeroStyles() {
    if (!this.props.periodical.image) {
      return {};
    }

    return {
      backgroundImage: 'linear-gradient(to right, rgba(0,0,0,.5) 50%, rgba(0,0,0,.2)),'
        + `url(${this.props.periodical.image})`,
    };
  }

  private renderHeadline() {
    return (this.props.periodical.headline)
      ? <h2 itemProp="headline" className="subtitle">{this.props.periodical.headline}</h2>
      : null;
  }

  private renderDescription() {
    return (this.props.periodical.description)
      ? <p itemProp="description" className="content">{this.props.periodical.description}</p>
      : null;
  }

  private renderSettingsLink() {
    const creator = this.props.periodical.creator;
    if (
      typeof this.props.periodical.datePublished !== 'undefined' &&
      (!creator || !this.props.session || !isPeriodicalOwner({ creator }, this.props.session))
    ) {
      return null;
    }

    return (
      <li>
        <Link
          to={`/journal/${this.props.periodical.identifier}/manage`}
          title="Configure your journal's name, description, etc."
        >
          Journal settings
        </Link>
      </li>
    );
  }

  private renderSubmissionButton() {
    if (!this.props.periodical || !this.props.periodical.datePublished) {
      return null;
    }

    return (
      <Link
        className="button is-primary is-large"
        to={`/journal/${this.props.periodical.identifier}/submit`}
        title="Submit your work for publication in this journal"
      >
        Submit a manuscript
      </Link>
    );
  }

  private renderBoard() {
    if (!this.props.periodical.creator || !this.props.periodical.creator.name) {
      return null;
    }

    const creatorList = (
      <span itemProp="creator" title={this.props.periodical.creator.identifier}>
        {this.props.periodical.creator.name}
      </span>
    );

    return (
      <section className="section">
        <div className="container">
          <p>
            This journal was founded by {creatorList}.
          </p>
        </div>
      </section>
    );
  }

  private renderArticles() {
    if (typeof this.props.articles === 'undefined') {
      // Still loading, don't display anything...
      return null;
    }

    if (this.props.articles === null) {
      return (
        <section className="section">
          <div className="container">
            <div className="message is-danger">
              <div className="message-body">
                Could not load this journal's articles, please try again.
              </div>
            </div>
          </div>
        </section>
      );
    }

    return this.renderArticleList(this.props.articles);
  }

  private renderArticleList(articles: Array<Partial<ScholarlyArticle>>) {
    if (this.props.periodical && !this.props.periodical.datePublished) {
      return (
        <section className="section" data-test-id="articleList">
          <div className="container content">
            <hr/>
            <h2 className="is-size-4">Articles</h2>
            <p>
              Once this journal is public, articles it publishes will be listed here.
            </p>
          </div>
        </section>
      );
    }

    if (articles.length === 0) {
      return (
        <section className="section" data-test-id="articleList">
          <div className="container content">
            <hr/>
            <h2 className="is-size-4">No articles</h2>
            <p>
              This journal has not published any articles yet.
              You can be the first;&nbsp;
              <Link
                to={`/journal/${this.props.periodical.identifier}/submit`}
                title="Submit your work for publication in this journal"
              >
                submit your manuscript
              </Link>.
            </p>
          </div>
        </section>
      );
    }

    const containedArticles: Array<Partial<ScholarlyArticle>> = articles.map((article) => ({
      ...article,
      isPartOf: {
        // If a periodical contains articles, it is public and thus has an identifier.
        // Since TypeScript doesn't know that, we need the `!` here.
        identifier: this.props.periodical.identifier!,
        url: this.props.url,
      },
    }));

    return (
      <section className="section" data-test-id="articleList">
        <div className="container content">
          <hr/>
          <div className="columns">
            <div className="column is-half-widescreen">
              <h2 className="is-size-4">Articles</h2>
              <ArticleList articles={containedArticles}/>
            </div>
          </div>
        </div>
      </section>
    );
  }

  private renderSuggestion() {
    const suggestion = this.getSuggestion();

    if (suggestion === null) {
      return null;
    }

    return (
      <section className="section">
        <div className="container">
          {suggestion}
        </div>
      </section>
    );
  }

  private getSuggestion() {
    if (!this.props.periodical.name) {
      return (
        <div className="message is-warning content">
          <div className="message-body">
            Your journal does not have a title yet. Give it a name in the&nbsp;
            <Link
              to={`/journal/${this.props.periodical.identifier}/manage`}
              title="Configure your journal's name, description, etc."
            >
              journal settings
            </Link>.
          </div>
        </div>
      );
    }

    if (!this.props.periodical.datePublished) {
      return (
        <div className="message is-warning content">
          <div className="message-body">
            Your journal is not public yet. You can make it public in the&nbsp;
            <Link
              to={`/journal/${this.props.periodical.identifier}/manage`}
              title="Configure your journal's name, description, etc."
            >
              journal settings
            </Link>.
          </div>
        </div>
      );
    }

    if (
      this.props.periodical.creator &&
      this.props.session &&
      isPeriodicalOwner({ creator: this.props.periodical.creator }, this.props.session)
    ) {
      return (
        <div className="message is-info content">
          <div className="message-body">
            This is your journal! Take advantage of new features as soon as they are introduced:&nbsp;
            <OutboundLink
              to="https://tinyletter.com/Flockademic"
              title="The Flockademic newsletter"
              eventLabel="newsletter"
            >
              subscribe to the newsletter.
            </OutboundLink>
          </div>
        </div>
      );
    }

    return null;
  }
}
