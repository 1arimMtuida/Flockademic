if (process.env.MAINTENANCE_MODE) {
    describe('The website', function(){
        it('should display a message about maintenance mode on the homepage', function(){
            browser.url('/');
    
            expect(browser.isExisting('.hero h1')).toBe(true);
            expect(browser.element('.hero h1').getText()).toBe('Flockademic is down for maintenance');
        });
    
        it('should display a message about maintenance mode on arbitrary pages', function(){
            browser.url('/arbitrary-page');
    
            expect(browser.isExisting('.hero h1')).toBe(true);
            expect(browser.element('.hero h1').getText()).toBe('Flockademic is down for maintenance');
        });
    });
}